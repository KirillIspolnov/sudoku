package org.kirillbogatikov.sudoku;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Бе Бе Бе и Бу Бу Бу
 * 
 * @author Кирилл Испольнов
 */
public class Solver {
    private static final String EMPTY_CELL_FLAG = "[._0]-";
    private int size;
    private int block_size;
    private int[][] map;
    
    /**
     * Инициализирует экземпляр класса для решения судоку размером n * n
     * @param n размер судоку
     */
    public Solver(int n) {
        map = new int[n][n];
        size = n;
        block_size = (int)Math.sqrt(size);
    }
    
    /**
     * Инициализирует поле решаемого судоку данными, передаваемыми через поток stream
     * 
     * @param stream поток, снабжающий программу данными о поле игры
     */
    public void setInput(InputStream stream) {
        Scanner scanner = new Scanner(stream);
        String data;
        for(int i = 0; i < size; i++) {
            data = scanner.nextLine().replaceAll(EMPTY_CELL_FLAG, "0");
            for(int j = 0; j < size; j++) {
                map[i][j] = data.charAt(j) - 48;
            }
        }
        scanner.close();
    }
    
    /**
     * Возвращает true, если строка поля, в которой находится ячейка не содержит числа n, false - в любом другом случае
     * 
     * @param cell порядковый номер ячейки
     * @param n проверяемое число
     * @return true, если строка поля, в которой находится ячейка не содержит числа n, false - в любом другом случае
     */
    private boolean isValidByRow(int cell, int n) {
        int row_index = cell / size;
        int[] row_array = map[row_index];
        for(int i : row_array) {
            if(n == i) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Возвращает true, если столбец поля, в котором находится ячейка не содержит числа n, false - в любом другом случае
     * @param cell порядковый номер ячейки
     * @param n проверяемое число
     * @return true, если столбец поля, в котором находится ячейка не содержит числа n, false - в любом другом случае
     */
    private boolean isValidByCol(int cell, int n) {
        int col_index = cell % size;
        for(int i = 0; i < map.length; i++) {
            if(map[i][col_index] == n) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Возвращает true, если блок поля, в котором находится ячейка не содержит числа n, false - в любом другом случае
     * @param cell порядковый номер ячейки
     * @param n проверяемое число
     * @return true, если блок поля, в котором находится ячейка не содержит числа n, false - в любом другом случае
     */
    private boolean isValidByBlc(int cell, int n) {
        int x = (cell / size) / block_size;
        int y = (cell % size) /  block_size;
        
        x *= block_size;
        y *= block_size;
        
        for(int i = 0; i < block_size; i++) {
            for(int j = 0; j < block_size; j++) {
                if(map[x + i][y + j] == n) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /**
     * Проверяет текущее поле на наличие пустых ячеек, что иллюстрирует решено судоку (пустых нет) или нет
     * 
     * @return true, если на поле отсутствуют пустые ячейки (т.е. судоку решено), false - в остальных случаях
     */
    public boolean isSolved() {
        for(int[] row : map) {
            for(int cell : row) {
                if(cell == 0)
                    return false;
            }
        }
        return true;
    }
    
    /**
     * Возвращает true, если проверяемое число n отсутствует в строке, столбце и блоке ячейки с номером cell
     * 
     * @param cell порядковый номер ячейки
     * @param n проверяемое число
     * @return Возвращает true, если проверяемое число n отсутствует в строке, столбце и блоке ячейки с номером cell, false - в остальных случаях
     */
    public boolean isValid(int cell, int n) {
        return isValidByRow(cell, n) & isValidByCol(cell, n) & isValidByBlc(cell, n);
    }
    
    /**
     * Возвращает значение ячейки
     *  
     * @param cell порядковый номер ячейки
     * @return значение ячейки
     */
    private int getCellValue(int cell) {
        return map[cell / size][cell % size];
    }
    
    /**
     * Устанавливает новое значение ячейки
     *  
     * @param cell порядковый номер ячейки
     * @param n значение ячейки
     */
    private void setCellValue(int cell, int n) {
        map[cell / size][cell % size] = n;
    }
    
    /**
     * Подбирает решение для ячейки
     * 
     * @param cell порядковый номер ячейки
     */
    private void solve(int cell) {
        for(int n = 1; n <= size; n++) {
            if(isValid(cell, n)) {
                setCellValue(cell, n);
                break;
            }
        }
    }
    
    /**
     * Решает судоку путём подбора знаяения для каждой пустой ячейки
     * 
     * @return решённое (или нет) судоку
     */
    public int[][] solve() {
        for(int i = 0; i < size * size; i++) {
            if(getCellValue(i) == 0) {
                solve(i);
            }
        }
        
        return map;
    }
}
