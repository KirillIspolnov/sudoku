package org.kirillbogatikov.sudoku;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Arrays;

public class Test {

    public static void main(String[] args) throws Throwable {
        Solver solver = new Solver(4);
        solver.setInput(new BufferedInputStream(new FileInputStream("input.txt")));
        
        int[][] map = solver.solve();
        
        for(int[] row : map)
            System.out.println(Arrays.toString(row));
    }

}
